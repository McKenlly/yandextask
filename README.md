# YandexTask
Тестовое задание для школы мобильной разработки от яндекса. В работе продемонстриован пример работы с библиотекой Rest API yandex disk.
## MainActivity
Происходит вход в яндекс диск по токену, который пользователь получает при авторизации. Помогает **SessionManager** -- сохраняет токен в SharedPreferences, чтобы при повторном запуске не требовалось вводить токен, а происходила автоматическая авторизация.
Если User захочет изменить почту(Яндекс.Диск) -- выйдет из меню и токен удалится. Если пользователь неправльно ввел токен или не подключился, то отображается соответствующее окно. 

 ![](https://preview.ibb.co/gZCJyn/photo_2018_05_01_13_41_14.jpg) ![](https://preview.ibb.co/m3GHPS/photo_2018_05_01_13_48_51.jpg) [](https://preview.ibb.co/gZCJyn/photo_2018_05_01_13_41_14.jpg")
 
 
В противном случае его встречает информация о **Yandex disk**.

![](https://preview.ibb.co/kji8W7/photo_2018_05_01_13_48_47.jpg)

## GalleryActivity
### GalleryFragment
Основан на фрагменте с использованием RecyclerView, Glide, Retrofit, AsynkTaskLoader. Фрагмент сохраняет свое состояние при паузе и переходе к другому фрагменту. AsynkTaskLoader автоматически подгружает все данные о картинках в размере 6 штук, чтобы поток в котором прорисовывалась галерея не простаивала.
Благодаря библиотеке Glide не пришлось использовать AsynkTask.
Загрузка на диск тоже предусмотрена, эта роль возлагается на FAB(Только картинки).

![](https://preview.ibb.co/iOTLdn/photo_2018_05_01_13_41_20.jpg)

При отключении интернета выдает соответсвующую ошибку.

![](https://preview.ibb.co/kbcFTn/photo_2018_05_02_09_47_05.jpg)

### Image DetailFragment

При нажатии на картинку открывается полное изображение картинки на ViewPager.
Вся информация о картинках передается списком через объект Bundle. Прусмотрено скачивание картинки и возможность посмотреть информацию о ней.

![](https://preview.ibb.co/h8rHPS/photo_2018_05_01_13_41_22.jpg) ![](https://preview.ibb.co/ideWZS/photo_2018_05_02_09_58_01.jpg)

Эта работа была довольно интересной, раньше не работал с яндекс диском. Оказалось все проще, чем я думал.

## Полезные ссылки.
+ https://www.viralandroid.com/search?updated-max=2016-05-01T12:08:00%2B05:45&max-results=7
+ https://code.tutsplus.com/ru/tutorials/code-an-image-gallery-android-app-with-glide--cms-28207
+ https://habrahabr.ru/post/280586/
+ https://www.programcreek.com/java-api-examples/?api=com.bumptech.glide.load.engine.GlideException
+ https://habrahabr.ru/post/314028/
+ http://easyandroid.ru/index.php?p=795
+ https://habrahabr.ru/post/349650/
+ https://habrahabr.ru/post/258195/
+ https://github.com/yandex-disk/yandex-disk-sdk-java