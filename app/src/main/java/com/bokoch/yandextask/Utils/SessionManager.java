package com.bokoch.yandextask.Utils;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;

public class SessionManager {

    public static String mToken;

    // LogCat tag
    private static String TAG = SessionManager.class.getSimpleName();

    private SharedPreferences mPref;

    private SharedPreferences.Editor mEditor;
    private Context context;

    int PRIVATE_MODE = 0;

    private static final String PREF_NAME = "session";

    private static final String TAG_TOKEN = "token";

    @SuppressLint("CommitPrefEdits")
    public SessionManager(Context context) {
        this.context = context;
        mPref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        mEditor = mPref.edit();
    }

    public void setToken(String token) {
        mToken = token;
        mEditor.putString(TAG_TOKEN, token).commit();
        Log.d(TAG, "User token session modified!");
    }

    public void removeToken() {
        mEditor.remove(TAG_TOKEN);
        mEditor.apply();
    }
    public String getToken() {
        String token = mPref.getString(TAG_TOKEN, "");
        return token;
    }

    public boolean isLogin(){
        Log.d(TAG, "Is Login user" );

        return !mPref.getString(TAG_TOKEN, "").equals("");
    }
}
