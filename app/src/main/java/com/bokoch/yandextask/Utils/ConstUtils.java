package com.bokoch.yandextask.Utils;


public class ConstUtils {
    public static final String CLIENT_ID = "5badd84c878443daaeeceaf6e5971763";
    public static final String AUTH_URL = "http://oauth.yandex.ru/authorize?response_type=token&client_id=" + CLIENT_ID;
    public static final String BASE_URL = "https://cloud-api.yandex.net/v1/disk/";

    public static class UploadUtils {
        public static final int GET_FILE_TO_UPLOAD = 100;
    }
    public static class FromGalleyFiles{
        public static final String IMAGE_DRAWABLE = "image_drawable";
        public static final String POSITION_IMAGE = "position_image";
        public static final String PHOTO = "photo";

    }
    public static class RequestUtils {

        public static final int LIMIT = 6;
        public static final String TYPE_GALLERY = "image";
        public static final String PREVIEW_SIZE = "M";
    }
}
