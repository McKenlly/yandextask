package com.bokoch.yandextask.Fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.UiThread;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.bignerdranch.expandablerecyclerview.ExpandableRecyclerAdapter;
import com.bokoch.yandextask.Adapters.ExpandbleAdapter.Skill;
import com.bokoch.yandextask.Adapters.ExpandbleAdapter.SkillAdapter;
import com.bokoch.yandextask.Helpers.JsonHelper;
import com.bokoch.yandextask.R;

import org.json.JSONException;

import java.io.IOException;
import java.util.List;

public class AboutMeFragment extends Fragment {
    public static String TAG = AboutMeFragment.class.getSimpleName();
    private List<Skill> mSkills;
    private SkillAdapter mAdapter;

    public static Fragment newInstance() {
        return new AboutMeFragment();
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().setTitle(getString(R.string.about_me));
        try {
            mSkills = JsonHelper.readPortfolioJSONFile(getContext());
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.recycler_view, container, false);
        RecyclerView recyclerView = v.findViewById(R.id.images_recyclerview);
        mAdapter = new SkillAdapter(getContext(), mSkills);

        recyclerView.setAdapter(mAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        setHasOptionsMenu(true);
        return v;
    }
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mAdapter.onSaveInstanceState(outState);
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.gallery_action_bar, menu);
        menu.findItem(R.id.update_action_bar_item).setVisible(false);
        menu.findItem(R.id.about_me_action_bar_item).setVisible(false);
        getActivity().invalidateOptionsMenu();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getFragmentManager().popBackStack();
                break;
            default:
                return super.onOptionsItemSelected(item);

        }
        return true;
    }
}
