package com.bokoch.yandextask.Fragments;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bokoch.yandextask.Adapters.ViewPagerAdapter;
import com.bokoch.yandextask.Models.ImageModel;
import com.bokoch.yandextask.R;
import com.bokoch.yandextask.Models.DownloadLink;
import com.bokoch.yandextask.Network.YandexDiskClientApi;
import com.bokoch.yandextask.Utils.SessionManager;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.bokoch.yandextask.Helpers.FileInfoHelper.getImageInfo;
import static com.bokoch.yandextask.Utils.ConstUtils.BASE_URL;
import static com.bokoch.yandextask.Utils.ConstUtils.FromGalleyFiles.PHOTO;
import static com.bokoch.yandextask.Utils.ConstUtils.FromGalleyFiles.POSITION_IMAGE;

public class ImageDetailFragment extends Fragment implements View.OnClickListener {
    public static String TAG = ImageDetailFragment.class.getSimpleName();
    private ViewPagerAdapter mViewPagerAdapter;
    private ViewPager mViewPager;
    private ArrayList<ImageModel> mImageModels;
    int mPosition;
    public static boolean mHasConnection;
    public static ImageDetailFragment newInstance(int position, List<ImageModel> images) {
        Bundle bundle = new Bundle();

        bundle.putInt(POSITION_IMAGE, position);
        bundle.putParcelableArrayList(PHOTO, (ArrayList<? extends Parcelable>) images);

        ImageDetailFragment fragment = new ImageDetailFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mImageModels = new ArrayList<>();
        mPosition = getArguments().getInt(POSITION_IMAGE);
        mImageModels =  getArguments().getParcelableArrayList(PHOTO);
        mViewPagerAdapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager(), mImageModels);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.image_detail_action_bar, menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.about_photo_action_bar_item:
                ShowInfo(getString(R.string.about_image),
                        getImageInfo(mImageModels.get(mViewPager.getCurrentItem())));
                break;
            case android.R.id.home:
                getFragmentManager().popBackStack();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.image_view_pager, container, false);
        mViewPager = (ViewPager) view.findViewById(R.id.container);

        FloatingActionButton floatingActionButton =(FloatingActionButton)
                                            view.findViewById(R.id.download_image_floating_button);
        mPosition = mPosition < 0 ? 0 : mPosition;
        getActivity().setTitle(mImageModels.get(mPosition).getName());
        floatingActionButton.setOnClickListener(this);
        mViewPager.setAdapter(mViewPagerAdapter);
        mViewPager.setCurrentItem(mPosition);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }
            @Override
            public void onPageSelected(int position) {
                //Для каждого изображения, свое имя.
                getActivity().setTitle(mImageModels.get(position).getName());
            }
            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        //Показывать меню
        setHasOptionsMenu(true);
        mHasConnection = true;

        return view;

    }

    private void ShowInfo(String title, String body) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(title)
                .setMessage(body)
                .setNegativeButton(getString(R.string.ok_button),
                        new DialogInterface.OnClickListener() { // Кнопка ОК
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss(); // Отпускает диалоговое окно
                            }
                        })
                .setCancelable(false);
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onClick(View v) {

        int position = mViewPager.getCurrentItem();
        position = position > 0 ? position : 0;

        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        YandexDiskClientApi clientApi = retrofit.create(YandexDiskClientApi.class);
        final Call<DownloadLink> call;
        Log.e(TAG + ": on try", mImageModels.get(position).getPath());
        call = clientApi.getLink(SessionManager.mToken, mImageModels.get(position).getPath());
        call.enqueue(new Callback<DownloadLink>() {

            @Override
            public void onResponse(Call<DownloadLink> call, Response<DownloadLink> response) {
                if (response.isSuccessful()) {
                    try {
                        Uri uri = Uri.parse(response.body().getHref());
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(uri);
                        getActivity().startActivity(intent);
                    } catch (Exception e) {
                        Log.e(TAG + ": onResponse", "Error. Link filed.");

                        new AlertDialog.Builder(getActivity())
                                .setTitle(R.string.failed_link)
                                .setMessage(e.getMessage())
                                .setPositiveButton("OK", null)
                                .create()
                                .show();
                    }
                }
            }

            @SuppressLint("CheckResult")
            @Override
            public void onFailure(Call<DownloadLink> call, Throwable t) {
                Log.e(TAG + ": onFailure", t.toString());
                Toasty.info(getContext(), getString(R.string.failed_download_image), Toast.LENGTH_LONG);
            }
        });
    }

}
