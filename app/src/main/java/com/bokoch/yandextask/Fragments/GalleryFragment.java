package com.bokoch.yandextask.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.app.NavUtils;
import android.support.v4.content.Loader;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bokoch.yandextask.Adapters.GalleryAdapter;
import com.bokoch.yandextask.Models.ImageModel;
import com.bokoch.yandextask.Network.ListImagesLoader;
import com.bokoch.yandextask.R;
import com.bokoch.yandextask.Utils.SessionManager;
import com.yandex.disk.rest.Credentials;

import java.util.ArrayList;
import java.util.List;

import static com.bokoch.yandextask.Utils.ConstUtils.UploadUtils.GET_FILE_TO_UPLOAD;

public class GalleryFragment extends Fragment implements
        LoaderManager.LoaderCallbacks<List<ImageModel>>, View.OnClickListener {

    public static final String TAG = GalleryFragment.class.getSimpleName();

    private ProgressBar mProgressBar;
    private FloatingActionButton mLoadFileToDiskButton;
    private Credentials mCredentials;
    private GalleryAdapter mAdapter;
    private RecyclerView mRecyclerView;
    private TextView mNotNetwork;


    public static GalleryFragment newInstance() {
        return new GalleryFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e(TAG, "onCreate");
        mCredentials = new Credentials("", SessionManager.mToken);
        //Чтобы при смене ориентации экрана этот фрагмент сохранял свое состояние.
        setRetainInstance(true);
        //Показывать меню
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.recycler_view, container, false);
        Log.e(TAG, "onCreateView");

        mNotNetwork = (TextView) v.findViewById(R.id.no_internet_textview);

        mRecyclerView =(RecyclerView) v.findViewById(R.id.images_recyclerview);

        mProgressBar = (ProgressBar) v.findViewById(R.id.loading_recycler_view);

        mLoadFileToDiskButton = (FloatingActionButton)v.findViewById(R.id.load_to_disk_fab);
        mLoadFileToDiskButton.startAnimation(AnimationUtils
                                                .loadAnimation(getContext(), R.anim.floating_action));

        mAdapter = new GalleryAdapter(this, new ArrayList<ImageModel>());
        mLoadFileToDiskButton.setOnClickListener(this);



        mRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0 || dy < 0 && mLoadFileToDiskButton.isShown()) {
                    mLoadFileToDiskButton.hide();
                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    mLoadFileToDiskButton.show();
                }

                super.onScrollStateChanged(recyclerView, newState);
            }
        });
        getLoaderManager().initLoader(0, null, this);

        return v;
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().setTitle(R.string.gallery_name);
        inflater.inflate(R.menu.gallery_action_bar, menu);
        Log.e(TAG, "onCreateOptionsMenu");

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.about_me_action_bar_item:
                getActivity()
                        .getSupportFragmentManager()
                        .beginTransaction()
                        .replace(android.R.id.content, AboutMeFragment.newInstance())
                        .addToBackStack(null)
                        .commit();
                break;
            case R.id.update_action_bar_item:
                //Обновление всех фотографий.
                RefreshAllData();
                break;
            case android.R.id.home:
                //возвращаемся назад к первой активности.
                NavUtils.navigateUpFromSameTask(getActivity());
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }
    //Обновление фотографий.
    @Override
    public void onSaveInstanceState(Bundle outState) {
        Log.e(TAG, "onSaveInstance");
        super.onSaveInstanceState(outState);
    }

    private void RefreshAllData() {
        Log.e(TAG, "RefreshAllData");
        mAdapter.setData(null);
        getLoaderManager().restartLoader(0, null, this);

    }
    @Override
    public void onClick(View view) {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        getActivity().startActivityForResult(Intent.createChooser(intent,
                "Select image"), GET_FILE_TO_UPLOAD);
    }


    @NonNull
    @Override
    public Loader<List<ImageModel>> onCreateLoader(int i, Bundle bundle) {
        Log.e(TAG, "onCreateLoader");
        NoNetworkHide();
        ProgressBarVisible();
        return new ListImagesLoader(getActivity(), mCredentials);
    }

    @Override
    public void onLoadFinished(Loader<List<ImageModel>> loader, List<ImageModel> imageModels) {
        Log.e(TAG, "onLoadFinished " + imageModels.size());
        if (imageModels.size() == 0) {
            NoNetworkVisible();
        } else {
            NoNetworkHide();
        }
        mAdapter.setData(imageModels);
        ProgressBarHide();
    }

    @Override
    public void onLoaderReset(@NonNull Loader<List<ImageModel>> loader) {
        Log.e(TAG, "onLoadReset");

    }

    private void NoNetworkVisible() {
        if (mNotNetwork.getVisibility() != View.VISIBLE) {
            mNotNetwork.setVisibility(View.VISIBLE);
        }
    }
    private void NoNetworkHide() {
        if (mNotNetwork.getVisibility() != View.GONE) {
            mNotNetwork.setVisibility(View.GONE);
        }
    }

    private void ProgressBarVisible() {
        if(mProgressBar != null){
            mProgressBar.setVisibility(View.VISIBLE);
        }
    }
    private void ProgressBarHide() {
        if(mProgressBar != null){
            mProgressBar.setVisibility(View.GONE);
        }
    }


}