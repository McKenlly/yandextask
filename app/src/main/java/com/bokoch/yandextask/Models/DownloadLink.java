package com.bokoch.yandextask.Models;

import com.google.gson.annotations.SerializedName;

public class DownloadLink {

    /**
     * Только в таком виде!
     * href : https://downloader.dst.yandex.ru/disk/...
     * method : GET
     * templated : false
     */
    @SerializedName("href")
    private String href;
    @SerializedName("method")
    private String method;
    @SerializedName("templated")
    private String templated;

    public String getHref() {
        return href;
    }

    public String getMethod() {
        return method;
    }

    public String getTemplated() {
        return templated;
    }

    @Override
    public String toString() {
        return "DownloadLink{" +
                "href='" + href + '\'' +
                ", method='" + method + '\'' +
                ", templated=" + templated +
                '}';
    }
}
