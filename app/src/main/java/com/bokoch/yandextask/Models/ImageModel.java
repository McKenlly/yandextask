package com.bokoch.yandextask.Models;

import android.os.Parcel;
import android.os.Parcelable;

import com.bokoch.yandextask.Helpers.FileInfoHelper;
import com.yandex.disk.rest.json.Resource;

public class ImageModel implements Parcelable {
    public static final Creator<ImageModel> CREATOR = new Creator<ImageModel>() {
        @Override
        public ImageModel createFromParcel(Parcel in) {
            return new ImageModel(in);
        }

        @Override
        public ImageModel[] newArray(int size) {
            return new ImageModel[size];
        }
    };
    private String mName;
    private String mPath;
    private String mMediaType;
    private String mDownloadLink;
    private String mPreview;
    private long mSize;
    private String mDate;

    public ImageModel(Resource resource) {
        mName = FileInfoHelper.removeExpansion(resource.getName());

        mPath = resource.getPath() != null ? resource.getPath().getPath() : null;
        mMediaType = resource.getMediaType();
        mPreview = resource.getPreview();
        mDate = resource.getCreated().toString();
        mSize = resource.getSize();
    }

    private ImageModel(Parcel in) {
        mName = in.readString();
        mPath = in.readString();
        mMediaType = in.readString();
        mSize = in.readLong();
    }

    @Override
    public String toString() {
        return "ImageModelList{" +
                "mName='" + mName + '\'' +
                ", mPath='" + mPath + '\'' +
                ", mMediaType='" + mMediaType + '\'' +
                ", mSize=" + mSize +
                '}';
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(mName);
        parcel.writeString(mPath);
        parcel.writeString(mMediaType);
        parcel.writeLong(mSize);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public long getSize() {
        return mSize;
    }

    public void setSize(long size) {
        mSize = size;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) { mName = name;}

    public String getPath() {
        return mPath;
    }


    public String getMediaType() {
        return mMediaType;
    }

    public String getDownloadLink() {
        return mDownloadLink;
    }

    public void setDownloadLink(String downloadLink) {
        this.mDownloadLink = downloadLink;
    }

    public String getPreview() {
        return mPreview;
    }

    public String getDate() {
        return mDate;
    }
}
