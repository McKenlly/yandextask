package com.bokoch.yandextask.Models;

import com.yandex.disk.rest.Credentials;
import com.yandex.disk.rest.json.Link;

import java.io.File;

public class UploadImageModel {
    private Link mUrl;
    private File mFile;
    private Credentials mCredentials;

    public UploadImageModel(Link url, File file, Credentials credentials) {
        mUrl = url;
        mFile = file;
        mCredentials = credentials;
    }
    public Link getLink() {
        return mUrl;
    }

    public void setLink(Link url) {
        mUrl = url;
    }

    public File getFile() {
        return mFile;
    }

    public void setFile(File file) {
        mFile = file;
    }

    public Credentials getCredentials() {
        return mCredentials;
    }

    public void setCredentials(Credentials credentials) {
        mCredentials = credentials;
    }
}
