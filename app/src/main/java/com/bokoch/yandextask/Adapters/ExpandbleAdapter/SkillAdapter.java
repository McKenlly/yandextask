package com.bokoch.yandextask.Adapters.ExpandbleAdapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.UiThread;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bignerdranch.expandablerecyclerview.ExpandableRecyclerAdapter;
import com.bokoch.yandextask.R;

import java.util.List;


public class SkillAdapter extends ExpandableRecyclerAdapter<Skill, PortfolioItem,
                                                            SkillViewHolder, ItemViewHolder> {


    private LayoutInflater mInflater;
    private List<Skill> mSkillList;

    public SkillAdapter(Context context, @NonNull List<Skill> skillList) {
        super(skillList);
        mSkillList = skillList;
        mInflater = LayoutInflater.from(context);
    }

    @UiThread
    @NonNull
    @Override
    public SkillViewHolder onCreateParentViewHolder(@NonNull ViewGroup parentViewGroup, int viewType) {
        View recipeView;
        recipeView = mInflater.inflate(R.layout.item_skill, parentViewGroup, false);
        return new SkillViewHolder(recipeView);
    }

    @UiThread
    @NonNull
    @Override
    public ItemViewHolder onCreateChildViewHolder(@NonNull ViewGroup childViewGroup, int viewType) {
        View itemSkill;
        itemSkill = mInflater.inflate(R.layout.item_portfolio, childViewGroup, false);
        return new ItemViewHolder(itemSkill);
    }

    @UiThread
    @Override
    public void onBindParentViewHolder(@NonNull SkillViewHolder recipeViewHolder, int parentPosition, @NonNull Skill skill) {
        recipeViewHolder.bind(skill);
    }

    @UiThread
    @Override
    public void onBindChildViewHolder(@NonNull ItemViewHolder itemSkillHolder,
                                      int parentPosition, int childPosition, @NonNull PortfolioItem item_skill) {
        itemSkillHolder.bind(item_skill);
    }
    @Override
    public int getParentViewType(int parentPosition) {
        return 1;
    }
    @Override
    public int getChildViewType(int parentPosition, int childPosition) {
        return 2;
    }

    @Override
    public boolean isParentViewType(int viewType) {
        return viewType == 1;
    }

}
