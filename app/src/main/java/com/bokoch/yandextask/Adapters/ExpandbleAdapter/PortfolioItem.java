package com.bokoch.yandextask.Adapters.ExpandbleAdapter;

public class PortfolioItem {

    private String mDescription;

    public PortfolioItem(String description) {
        mDescription = description;
    }

    public String getDescription() {
        return mDescription;
    }

}
