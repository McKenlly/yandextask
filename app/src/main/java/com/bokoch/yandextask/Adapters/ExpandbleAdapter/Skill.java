package com.bokoch.yandextask.Adapters.ExpandbleAdapter;
import com.bignerdranch.expandablerecyclerview.model.Parent;

import java.util.List;

public class Skill implements Parent<PortfolioItem> {

    private String mNameSkill;
    private List<PortfolioItem> mDescriptions;

    public Skill(String name, List<PortfolioItem> descriptions) {
        mNameSkill = name;
        mDescriptions = descriptions;
    }

    public String getNameSkill() {
        return mNameSkill;
    }

    @Override
    public List<PortfolioItem> getChildList() {
        return mDescriptions;
    }

    @Override
    public boolean isInitiallyExpanded() {
        return false;
    }

    public PortfolioItem getDescriptions(int position) {
        return mDescriptions.get(position);
    }

}
