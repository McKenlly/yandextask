package com.bokoch.yandextask.Adapters;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.bokoch.yandextask.Activities.GalleryActivity;
import com.bokoch.yandextask.Activities.MainActivity;
import com.bokoch.yandextask.Fragments.ImageDetailFragment;
import com.bokoch.yandextask.Models.ImageModel;
import com.bokoch.yandextask.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;


public class ViewPagerAdapter extends FragmentPagerAdapter {
    private List<ImageModel> mData;

    public ViewPagerAdapter(FragmentManager fragmentManager, ArrayList<ImageModel> data) {
        super(fragmentManager);
        mData = data;
    }

    @Override
    public Fragment getItem(int position) {
        return PlaceholderFragment.newInstance(position, mData.get(position));
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mData.get(position).getName();
    }


    public static class PlaceholderFragment extends Fragment {

        ImageModel mImageModel;

        int mPos;
        private static final String ARG_SECTION_NUMBER = "section_number";
        private static final String ARG_IMG = "image_title";

        public static PlaceholderFragment newInstance(int sectionNumber, ImageModel imageModel) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();

            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            args.putParcelable(ARG_IMG, imageModel);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public void setArguments(Bundle args) {
            super.setArguments(args);
            this.mPos = args.getInt(ARG_SECTION_NUMBER);
            mImageModel = args.getParcelable(ARG_IMG);
        }



        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.photo_detail, container, false);

            final ImageView imageView = (ImageView) rootView.findViewById(R.id.image_view_photo_detail);
            if (GalleryActivity.hasConnection(getActivity())) {
                GlideDownload(imageView);
            } else {
                Toast.makeText(getContext(), "NOOOOOOOOOOOOOOOOOOOO", Toast.LENGTH_SHORT)
                        .show();
            }
            return rootView;
        }


        private void GlideDownload(ImageView view) {
            Glide.with(getActivity())
                    .load(mImageModel.getDownloadLink())
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.loading_icon)
                            .diskCacheStrategy(DiskCacheStrategy.RESOURCE))
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            Log.e("Glide: onFailed", e.getMessage() );
                            Toasty.warning(getActivity(), getString(R.string.no_network), Toast.LENGTH_SHORT);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            return false;
                        }
                    })
                    .into(view);

        }

    }
}