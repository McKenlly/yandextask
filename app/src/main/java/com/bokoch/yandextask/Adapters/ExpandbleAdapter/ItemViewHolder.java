package com.bokoch.yandextask.Adapters.ExpandbleAdapter;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ChildViewHolder;
import com.bokoch.yandextask.R;

public class ItemViewHolder extends ChildViewHolder {

    private TextView mDescriptionTextView;

    public ItemViewHolder(@NonNull View itemView) {
        super(itemView);
        mDescriptionTextView = (TextView) itemView.findViewById(R.id.portfolio_item_textview);
    }

    public void bind(@NonNull PortfolioItem item) {
        mDescriptionTextView.setText(item.getDescription());
    }
}