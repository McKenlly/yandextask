package com.bokoch.yandextask.Adapters.ExpandbleAdapter;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ParentViewHolder;
import com.bokoch.yandextask.R;

public class SkillViewHolder extends ParentViewHolder {
    @NonNull
    private TextView mSkillTextView;

    public SkillViewHolder(@NonNull View itemView) {
        super(itemView);
        mSkillTextView = (TextView) itemView.findViewById(R.id.skill_item_textview);
    }

    public void bind(@NonNull Skill skill) {
        mSkillTextView.setText(skill.getNameSkill());
    }

}
