package com.bokoch.yandextask.Adapters;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.bokoch.yandextask.Fragments.ImageDetailFragment;
import com.bokoch.yandextask.Network.YandexDiskClientApi;
import com.bokoch.yandextask.Models.DownloadLink;
import com.bokoch.yandextask.Models.ImageModel;
import com.bokoch.yandextask.R;
import com.bokoch.yandextask.Utils.SessionManager;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.bokoch.yandextask.Utils.ConstUtils.BASE_URL;


public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.ViewHolder>{

    public static final String TAG = GalleryAdapter.class.getSimpleName();

    private Fragment mFragment;
    private List<ImageModel> mImageModels;

    @SuppressLint("UseSparseArrays")
    public GalleryAdapter(Fragment fragment, List<ImageModel> mImageModels){
        this.mFragment = fragment;
        this.mImageModels = mImageModels;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View imageView;

        imageView = LayoutInflater.from(mFragment.getContext())
                .inflate(R.layout.item_photo, parent, false);

        return new ViewHolder(imageView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        if(position >= 0 && position < mImageModels.size()){
            //Если ссылки на скачивание картинки нет, то делаем запрос.
            if (mImageModels.get(position).getDownloadLink() != null) {
                DownloadImageGlide(position, holder);
            } else {
                mImageModels.get(position).setDownloadLink("");
                LoadLink(mImageModels.get(position), position, holder);
            }
        }
    }
    public void setData(List<ImageModel> data) {
        mImageModels.clear();
        if (data != null) {
            mImageModels.addAll(data);
        }
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return mImageModels.size();
    }

    private void LoadLink(final ImageModel item, final int position, final GalleryAdapter.ViewHolder holder) {


        final Retrofit retrofit = new Retrofit.Builder()
                                              .baseUrl(BASE_URL)
                                              .addConverterFactory(GsonConverterFactory.create())
                                              .build();
        YandexDiskClientApi clientApi = retrofit.create(YandexDiskClientApi.class);

        final Call<DownloadLink> call = clientApi.getDownloadFileLink("OAuth " +
                                                            SessionManager.mToken, item.getPath());
        call.enqueue(new Callback<DownloadLink>() {
            @Override
            public void onResponse(Call<DownloadLink> call, Response<DownloadLink> response) {
                if (response.isSuccessful()) {
                    Log.e(TAG + ": onResponse", response.body().toString());
                    item.setDownloadLink(response.body().getHref());
                    DownloadImageGlide(position, holder);
                }
            }
            @Override
            public void onFailure(Call<DownloadLink> call, Throwable t) {
                Log.e(TAG + ": onFailure", t.toString());
                Toasty.warning(mFragment.getContext(),
                        mFragment.getContext().getString(R.string.no_network), Toast.LENGTH_LONG).show();

            }
        });
    }


    private void DownloadImageGlide(final int position, final GalleryAdapter.ViewHolder holder) {
        Log.e(TAG, "DownloadGlideImage");
        Glide.with(mFragment)
                .load(mImageModels.get(position).getDownloadLink())
                .thumbnail(.9999f)
                .apply(new RequestOptions()
                        .error(R.drawable.error_loading_icon)
                        .placeholder(R.drawable.loading_icon)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                )
                .into(holder.mImageView).getRequest();

    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView mImageView;

        private ViewHolder(View itemView) {
            super(itemView);
            Log.e(TAG, "ViewHolder constructor");
            this.mImageView = (ImageView) itemView.findViewById(R.id.iv_photo);

            mImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                        openFullImage(getAdapterPosition());
                }
            });
        }

        private void openFullImage(int pos) {
            Fragment photoFragment = ImageDetailFragment.newInstance(pos, mImageModels);
            mFragment.getActivity()
                    .getSupportFragmentManager()
                    .beginTransaction()
                    .replace(android.R.id.content, photoFragment)
                    .addToBackStack(null)
                    .commit();
        }
    }

}
