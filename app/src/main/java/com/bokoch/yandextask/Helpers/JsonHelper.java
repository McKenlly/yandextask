package com.bokoch.yandextask.Helpers;

import android.content.Context;

import com.bokoch.yandextask.Adapters.ExpandbleAdapter.PortfolioItem;
import com.bokoch.yandextask.Adapters.ExpandbleAdapter.Skill;
import com.bokoch.yandextask.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class JsonHelper {
    public static List<Skill> readPortfolioJSONFile(Context context) throws IOException,JSONException {

        String jsonText = readText(context, R.raw.my_portfolio);

        List<Skill> outputList = new ArrayList<>();
        JSONArray jsonRoot = new JSONArray(jsonText);
        for (int i = 0; i < jsonRoot.length(); i++) {
            JSONObject jsonObj = jsonRoot.getJSONObject(i);
            List<PortfolioItem> listItem = new ArrayList<>();
            JSONArray portfolioItem = jsonObj.getJSONArray("List");
            for (int j = 0; j < portfolioItem.length(); j++) {
                listItem.add(new PortfolioItem(portfolioItem.getString(j)));
            }
            Skill skill = new Skill(jsonObj.getString("nameLabel"), listItem);
            outputList.add(skill);
        }
        return outputList;
    }



    private static String readText(Context context, int resId) throws IOException {
        InputStream is = context.getResources().openRawResource(resId);
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String s = null;
        while((  s = br.readLine())!=null) {
            sb.append(s);
            sb.append("\n");
        }
        return sb.toString();
    }

}
