package com.bokoch.yandextask.Helpers;

import com.bokoch.yandextask.Models.ImageModel;
import com.yandex.disk.rest.json.DiskInfo;

import java.text.DecimalFormat;

public class FileInfoHelper {
    public static String getImageInfo(ImageModel imageModel) {
        String name = "Name: " + imageModel.getName() + "\n";
        String path = "Path: " + imageModel.getPath() + "\n";
        String type = "MediaType: " + imageModel.getMediaType() + "\n";
        String size = "Size: " + ConvertFileSize(imageModel.getSize()) + "\n";
        String date = "Date: " + imageModel.getDate() + "\n";
        return name + path + type + size + date;
    }
    public static String ConvertFileSize(long size) {
        if(size <= 0) return "0";
        final String[] units = new String[] { "B", "KB", "MB", "GB", "TB" };
        int digitGroups = (int) (Math.log10(size)/Math.log10(1024));
        return new DecimalFormat("#,##0.#").format(size/Math.pow(1024, digitGroups)) + " " + units[digitGroups];
    }

    public static String getDiskInfo(DiskInfo diskInfo) {
        if (diskInfo == null) {
            return "Error";
        }
        String freeSize = "Used space: " + ConvertFileSize(diskInfo.getUsedSpace()) + "\n";
        String totalSize ="Total space: " + ConvertFileSize(diskInfo.getTotalSpace()) + "\n";
        return  freeSize + totalSize;
    }
    public static String removeExpansion(String fileName) {
        int pos = fileName.lastIndexOf('.');
        return fileName.substring(0, pos);
    }
}
