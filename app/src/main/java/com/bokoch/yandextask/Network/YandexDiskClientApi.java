package com.bokoch.yandextask.Network;

import com.bokoch.yandextask.Models.DownloadLink;
import com.yandex.disk.rest.json.Link;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;


public interface YandexDiskClientApi {
    //Скачивание файла. Хорошая статья: http://startandroid.ru/ru/blog/506-retrofit.html
    @GET("resources/download")
    Call<DownloadLink> getDownloadFileLink(@Header("Authorization") String token, @Query("path") String path);

    @GET("resources/download")
    Call<DownloadLink> getLink(@Header("Authorization") String token, @Query("path") String path);

    @GET("resources/upload")
    Call<Link> LoadFileToDisk(@Header("Authorization")String token, @Query("path") String path);
}
