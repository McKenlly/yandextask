package com.bokoch.yandextask.Network;

import android.os.AsyncTask;

import com.bokoch.yandextask.Utils.SessionManager;
import com.yandex.disk.rest.Credentials;
import com.yandex.disk.rest.RestClient;
import com.yandex.disk.rest.exceptions.ServerException;
import com.yandex.disk.rest.json.DiskInfo;

import java.io.IOException;

public class DiskInfoAsynkTask extends AsyncTask<Void, Void, DiskInfo> {

    @Override
    protected DiskInfo doInBackground(Void... voids) {
        DiskInfo diskInfo = null;
        RestClient client;
        client = new RestClient(new Credentials("", SessionManager.mToken));
        try {
            diskInfo = client.getDiskInfo();
        } catch (IOException | ServerException ex) {
            diskInfo = null;
        }

        return diskInfo;
    }
}