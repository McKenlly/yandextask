package com.bokoch.yandextask.Network;

import android.os.AsyncTask;
import android.util.Log;

import com.bokoch.yandextask.Models.UploadImageModel;
import com.yandex.disk.rest.Credentials;
import com.yandex.disk.rest.RestClient;
import com.yandex.disk.rest.exceptions.ServerException;
import com.yandex.disk.rest.json.Link;

import java.io.File;
import java.io.IOException;

import static com.bokoch.yandextask.Fragments.GalleryFragment.TAG;

public class UploadImageAsynkTask extends AsyncTask<UploadImageModel, Integer, Void> {


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(UploadImageModel... uploadImageModels) {
        UploadImageModel uploaderWrapper = uploadImageModels[0];

        Credentials credentials = uploaderWrapper.getCredentials();
        RestClient client = RestClientUtil.getInstance(credentials);

        File file = uploaderWrapper.getFile();
        Link link = uploaderWrapper.getLink();

        try {
            client.uploadFile(link, true, file, null);
        } catch (IOException | ServerException e) {
            Log.i(TAG, "doInBackground: " + e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

}
