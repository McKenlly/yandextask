package com.bokoch.yandextask.Network;

import android.content.Context;
import android.os.Handler;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;


import com.bokoch.yandextask.Models.ImageModel;
import com.yandex.disk.rest.Credentials;
import com.yandex.disk.rest.ResourcesArgs;
import com.yandex.disk.rest.ResourcesHandler;
import com.yandex.disk.rest.RestClient;
import com.yandex.disk.rest.exceptions.ServerException;
import com.yandex.disk.rest.json.Resource;
import com.yandex.disk.rest.json.ResourceList;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.bokoch.yandextask.Utils.ConstUtils.RequestUtils.LIMIT;
import static com.bokoch.yandextask.Utils.ConstUtils.RequestUtils.PREVIEW_SIZE;
import static com.bokoch.yandextask.Utils.ConstUtils.RequestUtils.TYPE_GALLERY;

public class ListImagesLoader extends AsyncTaskLoader<List<ImageModel>> {
    private static String TAG = ListImagesLoader.class.getSimpleName();


    private Credentials mCredentials;
    private Handler mHandler;
    private List<ImageModel> mImageModels;
    private boolean mCancelled;

    public ListImagesLoader(Context context, Credentials credentials) {
        super(context);
        mHandler = new Handler();
        mCredentials = credentials;
        mCancelled = false;
    }


    @Override
    public List<ImageModel> loadInBackground() {
        Log.e(TAG, "loadInBackground");
        int offset = 0;
        RestClient client = RestClientUtil.getInstance(mCredentials);
        mImageModels = new ArrayList<>();
        try {
            int currentSize = 0;
            do {

                ResourceList res = client.getFlatResourceList(new ResourcesArgs.Builder()
                        .setMediaType(TYPE_GALLERY)
                        .setLimit(LIMIT)
                        .setOffset(offset)
                        .setPreviewSize(PREVIEW_SIZE) // 150 пикселей
                        .setParsingHandler(new ResourcesHandler() {
                            @Override
                            public void handleItem(Resource resource) {
                                if (resource != null) {
                                    mImageModels.add(new ImageModel(resource));
                                }
                            }
                        })
                        .build());
                offset += LIMIT;
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        //Получает и возвращает промежуточный результат работы загрузчика.
                        if (mImageModels != null) {
                            deliverResult(new ArrayList<>(mImageModels));
                        }
                    }
                });//Происходит до тех пор, пока количество новых файлов, не будет равно текущему.
                currentSize = res.getItems().size();

            } while (!mCancelled && LIMIT <= currentSize);

            return mImageModels;
        } catch (IOException | ServerException ex) {
            Log.d(TAG, TAG + ": loadInBackground", ex);
        }
        return  mImageModels;
    }

    @Override
    public void deliverResult(List<ImageModel> newPhotoListItems) {
        Log.e(TAG, "deliverResult");
        if (isReset()) {
            if (newPhotoListItems != null) {
                onReleaseResources(newPhotoListItems);
            }
        }
        List<ImageModel> oldPhotos = mImageModels;
        mImageModels = newPhotoListItems;

        if (isStarted()) {
            super.deliverResult(newPhotoListItems);
        }


        if (oldPhotos != null) {
            onReleaseResources(oldPhotos);
        }
    }
    @Override
    protected void onStartLoading() {
        Log.e(TAG, "onStartLoading");
        if (mImageModels != null) {
            deliverResult(mImageModels);
        } else {
            forceLoad();
        }
    }
    @Override
    protected void onStopLoading() {
        Log.e(TAG, "onStopLoading");
        cancelLoad();
    }
    @Override
    public void onCanceled(List<ImageModel> imageModels) {
        super.onCanceled(imageModels);
        Log.e(TAG, "onCanceled");
        onReleaseResources(imageModels);
    }

    @Override protected void onReset() {
        super.onReset();
        Log.e(TAG, "onReset");

        onStopLoading();

        if (mImageModels != null) {
            onReleaseResources(mImageModels);
            mImageModels = null;
        }

    }
    private void onReleaseResources(List<ImageModel> photoListItems) {
        //для курсора
        Log.e(TAG, "onReleaseResources");
    }
}
