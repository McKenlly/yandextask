package com.bokoch.yandextask.Activities;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.bokoch.yandextask.R;
import com.bokoch.yandextask.Network.DiskInfoAsynkTask;
import com.bokoch.yandextask.Utils.SessionManager;
import com.yandex.disk.rest.json.DiskInfo;

import java.util.concurrent.ExecutionException;

import es.dmoral.toasty.Toasty;

import static com.bokoch.yandextask.Helpers.FileInfoHelper.getDiskInfo;
import static com.bokoch.yandextask.Utils.ConstUtils.AUTH_URL;

public class MainActivity extends FragmentActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    public static final String FRAGMENT_TAG = "list";
    private SessionManager mSessionManager;
    private Button mGetTokenButton;
    private Button mSignInButton;
    private EditText mTokenEditText;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth_sign_in);
        mGetTokenButton = (Button) findViewById(R.id.get_token_button);
        mSignInButton = (Button)  findViewById(R.id.sign_in_button);
        mTokenEditText = (EditText) findViewById(R.id.token_edit_text);
        mSessionManager = new SessionManager(getApplicationContext());
        if (mSessionManager.isLogin()) {
            mTokenEditText.setText(mSessionManager.getToken());
        }
    }

    public void startActivity() {
        Intent intent = new Intent(getApplicationContext(), GalleryActivity.class);
        startActivity(intent);
    }


    public void GetTokenClick(View view) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(AUTH_URL));
        startActivity(intent);
    }

    @SuppressLint("CheckResult")
    public void SignInClick(View view) {
        String token = mTokenEditText.getText().toString();
        if (TextUtils.isEmpty(token)) {
            Toasty.warning(getApplicationContext(), getString(R.string.empty_token),
                                    Toast.LENGTH_SHORT, false).show();
        } else {
            if (mSessionManager == null) {
                mSessionManager = new SessionManager(MainActivity.this);
            }
            mSessionManager.setToken(token);


            DiskInfoAsynkTask diskInfoAsynkTask= new DiskInfoAsynkTask();
            diskInfoAsynkTask.execute();
            DiskInfo diskInfo = null;

            try {
                diskInfo = diskInfoAsynkTask.get();
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }

            if (diskInfo != null) {
                createDiskInfoAlertDialog(getString(R.string.about_disk), getDiskInfo(diskInfo));
            } else {
                Toasty.error(getApplicationContext(), getString(R.string.sign_error),
                                                                        Toast.LENGTH_SHORT).show();
            }
        }
    }
    private void createDiskInfoAlertDialog(String title, String content) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle(title)
                .setMessage(content)
                .setPositiveButton(getString(R.string.ok_button),
                        new DialogInterface.OnClickListener() { // Кнопка ОК
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                startActivity();
                            }
                        })
                .setNegativeButton(getString(R.string.cancel_button),
                        new DialogInterface.OnClickListener() { // Кнопка ОК
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }
}
