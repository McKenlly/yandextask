package com.bokoch.yandextask.Activities;


import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.bokoch.yandextask.Fragments.GalleryFragment;
import com.bokoch.yandextask.R;
import com.bokoch.yandextask.Models.UploadImageModel;
import com.bokoch.yandextask.Network.UploadImageAsynkTask;
import com.bokoch.yandextask.Network.YandexDiskClientApi;
import com.bokoch.yandextask.Utils.SessionManager;
import com.yandex.disk.rest.Credentials;
import com.yandex.disk.rest.json.Link;

import java.io.File;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.bokoch.yandextask.Utils.ConstUtils.BASE_URL;
import static com.bokoch.yandextask.Utils.ConstUtils.UploadUtils.GET_FILE_TO_UPLOAD;

public class GalleryActivity extends AppCompatActivity {
    public final String FRAGMENT_TAG = GalleryActivity.class.getSimpleName();

    public final int REQUEST_CODE_PERMISSION = 1;

    protected Fragment newInstance() {
        return new GalleryFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);
        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.fragment_container);
        if (fragment == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(android.R.id.content, GalleryFragment.newInstance(),
                            GalleryFragment.TAG)
                    .commit();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case GET_FILE_TO_UPLOAD: {
                if (resultCode == Activity.RESULT_OK && data != null) {
                    CheckStoragePermission();
                    Uri uri = data.getData();
                    assert uri != null;
                    File file = new File(getPath(uri));
                    if (file.exists()) {
                        UploadImageToDisk(file);
                    } else {
                        Toast.makeText(getApplicationContext(),
                                getString(R.string.image_not_exist), Toast.LENGTH_LONG).show();
                    }
                }
                break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void UploadImageToDisk(final File file) {
        final Credentials credentials = new Credentials("", SessionManager.mToken);

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create());

        final Retrofit retrofit = builder.build();

        YandexDiskClientApi diskClientApi = retrofit.create(YandexDiskClientApi.class);
        final Call<Link> call = diskClientApi.LoadFileToDisk("OAuth " +
                                                        SessionManager.mToken, file.getName());

        call.enqueue(new Callback<Link>() {
            @Override
            public void onResponse(Call<Link> call, final Response<Link> response) {

                if(response.message().equals("OK")) {

                    new UploadImageAsynkTask()
                            .execute(new UploadImageModel(response.body(), file, credentials));
                } else {
                    Toasty.info(getApplicationContext(), getString(R.string.image_exist), Toast.LENGTH_SHORT).show();
                }

            }
            @Override
            public void onFailure(Call<Link> call, Throwable t) {
                Toasty.warning(getApplicationContext(), getString(R.string.no_network), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public String getPath(Uri uri) {
        String path = null;
        String[] projection = {MediaStore.Files.FileColumns.DATA};
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);

        if (cursor == null) {
            path = uri.getPath();
        } else {
            cursor.moveToFirst();
            int column_index = cursor.getColumnIndexOrThrow(projection[0]);
            path = cursor.getString(column_index);
            cursor.close();
        }

        return ((path == null || path.isEmpty()) ? (uri.getPath()) : path);
    }
    public void CheckStoragePermission() {
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                                 Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_CODE_PERMISSION);
        }
    }
    public static boolean hasConnection(final Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifiInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiInfo != null && wifiInfo.isConnected()) {
            return true;
        }
        wifiInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (wifiInfo != null && wifiInfo.isConnected()) {
            return true;
        }
        wifiInfo = cm.getActiveNetworkInfo();
        if (wifiInfo != null && wifiInfo.isConnected()) {
            return true;
        }
        return false;
    }
}